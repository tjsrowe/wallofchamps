<?php 
function compareCategoryOrder($cat1, $cat2)
{
    global $categories;
    
    $psn1 = getPositionOfKey($cat1, $categories);
    $psn2 = getPositionOfKey($cat2, $categories);
    
    if ($psn1 < $psn2) {
        return 1;
    } else if ($psn1 > $psn2) {
        return - 1;
    }
    return 0;
}
?>