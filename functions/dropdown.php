<?php 
function dropdown($var, $values, $defaultValue)
{
    out('<select name=' . $var . '>' . LF);
    $currentVar = $_GET[$var];
    if (NULL == $currentVar) {
        $currentVar = $defaultValue;
    }
    
    foreach ($values as $key => $val) {
        if (! strcmp($currentVar, $key)) {
            $selected = ' selected="selected"';
        } else {
            $selected = '';
        }
        out('<option value="' . $key . '"' . $selected . '>' . $val . '</option>');
    }
    out('</select>' . LF);
}
?>