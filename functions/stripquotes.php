<?php 
function stripquotes($string)
{
    if (substr($string, 0, 1) == '"' && substr($string, strlen($string) - 1, 1) == '"') {
        return substr($string, 1, strlen($string) - 2);
    }
    return trim($string);
}
?>