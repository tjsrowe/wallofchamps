<?php
require_once(FUNCTION_DIR . '/compareCategoryOrder.php');
require_once(FUNCTION_DIR . '/getPositionOfKey.php');
require_once(FUNCTION_DIR . '/stripquotes.php');
require_once(FUNCTION_DIR . '/dropdown.php');
require_once(FUNCTION_DIR . '/isAthleteView.php');

function championSortByYear($first, $second)
{
    if ($first->year < $second->year) {
        return - 1;
    }
    
    if ($first->year > $second->year) {
        return 1;
    }
    
    if (strcmp($first->specialty, $second->specialty) != 0) {
        return strcmp($first->specialty, $second->specialty);
    }
    
    if (strcmp($first->country, $second->country) != 0) {
        return strcmp($first->country, $second->country);
    }
    
    $catOrder = compareCategoryOrder($first->category, $second->category);
    if ($catOrder != 0) {
        return $catOrder;
    }
    
    return 0;
}

function championSortByCategory($first, $second)
{
    $catOrder = compareCategoryOrder($first->category, $second->category);
    if ($catOrder != 0) {
        return $catOrder;
    }
    
    if ($first->year < $second->year) {
        return - 1;
    }
    
    if ($first->year > $second->year) {
        return 1;
    }
    
    if (strcmp($first->specialty, $second->specialty) != 0) {
        return strcmp($first->specialty, $second->specialty);
    }
    
    if (strcmp($first->country, $second->country) != 0) {
        return strcmp($first->country, $second->country);
    }
    
    return 0;
}

function championSortByName($first, $second)
{
    if (strcmp($first->surname, $second->surname) != 0) {
        return strcmp($first->surname, $second->surname);
    }
    
    if (strcmp($first->firstname, $second->firstname) != 0) {
        return strcmp($first->firstname, $second->firstname);
    }
    
    if ($first->year < $second->year) {
        return - 1;
    }
    
    if ($first->year > $second->year) {
        return 1;
    }
    
    if (strcmp($first->specialty, $second->specialty) != 0) {
        return strcmp($first->specialty, $second->specialty);
    }
    
    if (strcmp($first->country, $second->country) != 0) {
        return strcmp($first->country, $second->country);
    }
    
    $catOrder = compareCategoryOrder($first->category, $second->category);
    if ($catOrder != 0) {
        return $catOrder;
    }
    
    return 0;
}

function out($str)
{
    echo ($str);
}

function currentVar($var, $defaultValue)
{
    if (!isset($_GET[$var])) {
        return $defaultValue;
    }
    $value = $_GET[$var];
    if (NULL == $value) {
        return $defaultValue;
    }
    return $value;
}

?>