<?php
function getPositionOfKey($key, &$array)
{
    $i = 0;
    foreach ($array as $currentKey => $currentValue) {
        if (strcmp($key, $currentKey) == 0) {
            return $i;
        }
        $i ++;
    }
    return - 1;
}

?>