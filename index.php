<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');


    define('LF', "\n");
	define('FUNCTION_DIR', 'functions');
	
	define('DEFAULT_SPECIALTY', 'XCO');
	define('DEFAULT_LOCALITY', 'AUS');
	define('DEFAULT_GROUP', 'year');
	
	require_once(FUNCTION_DIR . '/functions.php');
	
	$categories = Array (
		'EM' => 'Elite Men',
		'EW' => 'Elite Women',
		'23M' => 'Under 23 Male',
		'23W' => 'Under 23 Women',
		'19M' => 'Under 19 Male',
		'JM' => 'Junior Men',
		'19W' => 'Under 19 Women',
		'JW' => 'Junior Women',
		'17M' => 'Under 17 Male',
		'17W' => 'Under 17 Women',
		'15M' => 'Under 15 Male',
		'15W' => 'Under 15 Women',
		'13M' => 'Under 13 Male',
		'M30' => 'Veteren Men',
		'W30' => 'Veteren Women',
		'M40' => 'Masters Men',
		'W40' => 'Masters Women',
		'M50' => 'Super Masters Men',
		'W50' => 'Super Masters Women',
		'M60' => 'Grand Masters Men',
		'W60' => 'Grand Masters Women',
		'M70' => 'Ultra Masters Men',
		'XM' => 'Expert Men',
		'XW' => 'Expert Women',
		'OM' => 'Open Men',
		'SSO' => 'Singlespeed Open',
		'SM' => 'Sport Men',
		'OW' => 'Open Women',
		'SMA' => 'Sport Men A',
		'SMB' => 'Sport Men B',
		'SSM' => 'Singlespeed Men',
		'SW' => 'Sport Women',
		'SWA' => 'Sport Women A',
		'SWB' => 'Sport Women B',
		'P26' => 'Pro 26"',
		'P20' => 'Pro 20"',
		'E' => 'Expert',
		'S' => 'Sport',
		'N' => 'Novice',
		'H' => 'Hardtail',
		'MH' => 'Mens Hardtail',
		'SH' => 'Senior Hardtail',
		'JH' => 'Junior Hardtail',
		'18M' => 'Mens Under 18',
		'MU23' => 'Mens Under 23',
		'WU23' => 'Mens Under 23',
		'M18-24' => 'Mens 18-24',
		'W18-24' => 'Womens 18-24',
		'M25-29' => 'Mens 25-29',
		'W25-29' => 'Womens 25-29',
		'M30-34' => 'Mens 30-34',
		'W30-34' => 'Womens 30-34',
		'M35-39' => 'Mens 35-39',
		'W35-39' => 'Womens 35-39',
		'M40-44' => 'Mens 40-44',
		'W40-44' => 'Womens 40-44',
		'M45-49' => 'Mens 45-49',
		'W45-49' => 'Womens 45-49',
		'M50-59' => 'Mens 50-59',
		'W50-59' => 'Womens 50-59'
	);
	
	$specialties = Array(
		'XCO' => 'Cross Country Olympic',
		'DH' => 'Downhill',
		'4X' => 'Four-Cross',
		'XCM' => 'Cross Country Marathon',
		'S24' => 'Solo 24 Hour',
		'XCC' => 'Cross Country Criterium',
		'OT' => 'Observed Trials',
		'DS' => 'Dual Slalom',
		'XCR' => 'Cross Country Relay'
//		'SD' => 'Super-D',
//		'XCT' => 'Cross Country Time Trial',
//		'XCP' => 'Cross Country Point to Point',
//		'XCE' => 'Cross Country Eliminator',
//		'XCEN' => 'Cross Country Endurance',
	);
	
	$localities = Array(
		'AUS' => 'Australian Championships',
		'UCI' => 'UCI World Championships',
		'24HOA' => '24 Hours of Adrenaline World Championships',
		'WEMBO' => 'WEMBO World Championships',
		'VIC' => 'Victorian State Championships',
		'NSW' => 'NSW State Championships',
		'QLD' => 'Queensland State Championships',
		'O1' => 'Oceanic Championships',
		'CYG' => 'Commonwealth Youth Games'
	);

	
	class Champion {
		public $year;
		public $specialty;
		public $country;
		public $category;
		public $firstname;
		public $surname;
		public $state;
		
		function __construct() {
		}
	}

	$championsFileLocation = "./auschamps.csv";
	
	$championsFile = file($championsFileLocation);
	$lines = count($championsFile);
	
	$championsArray = Array();
	$states = Array();
	
	for ($currentLine = 0;$currentLine < $lines;$currentLine++) { 
		$currentChampLine = $championsFile[$currentLine];
		
		$lineParts = explode("\t", $currentChampLine);
		$champ = new Champion();
		$champ->year = intval(stripquotes($lineParts[0]));
		$champ->specialty = stripquotes($lineParts[1]);
		$champ->country = stripquotes($lineParts[2]);
		if (!in_array($champ->country, $states)) {
			$states[] = $champ->country;
		}
		$champ->category = stripquotes($lineParts[3]);
		$champ->firstname = stripquotes($lineParts[4]);
		$champ->surname = stripquotes($lineParts[5]);
		$champ->state = stripquotes(trim($lineParts[6]));
		
		$championsArray[] = $champ;
	}
	
	if (!isset($_GET['group'])) {
	    $groupByYear = false;
	    $groupByCategory = false;
	    $groupByAthlete = false;
	} else {
    	$groupByYear = strcmp('year', $_GET['group']) == 0 ? true : false;
        $groupByCategory = strcmp('cat', $_GET['group']) == 0 ? true : false;
        $groupByAthlete = strcmp('athlete', $_GET['group']) == 0 ? true : false;
	}
	
	if ($groupByYear) {
		uasort($championsArray, 'championSortByYear');
		$championsArray = array_reverse($championsArray);
	} else if ($groupByCategory) {
		uasort($championsArray, 'championSortByCategory');
		$championsArray = array_reverse($championsArray);
	} else if ($groupByAthlete) {
		uasort($championsArray, 'championSortByName');
	}
	
	out('<form method="GET">' . LF);
	dropdown('loc', $localities, DEFAULT_LOCALITY);
	dropdown('spec', $specialties, DEFAULT_SPECIALTY);
	dropdown('group', Array('year' => 'Year', 'cat' => 'Category', 'athlete' => 'Athlete'), DEFAULT_GROUP);
	out('<input type="submit">' . LF);
	out('</form>' . LF);
	
	$specialty = currentVar('spec', DEFAULT_SPECIALTY);
	$locality = currentVar('loc', DEFAULT_LOCALITY);
	
	$showSpecialty = strcmp('ALL', $specialty) == 0 || isAthleteView() ? true : false;
	
	$lastYear = NULL;
	$lastCategory = NULL;
	$lastRiderName = NULL;
	
	$lastGroupId = NULL;
	
	$columnHeadings = Array();
	if ($groupByYear) {
		if ($showSpecialty) {
			$columnHeadings[] = 'Specialty';
		}
		$columnHeadings[] = 'Category';
		$columnHeadings[] = 'Rider';
	} else if ($groupByCategory) {
		if ($showSpecialty) {
			$columnHeadings[] = 'Specialty';
		}
		$columnHeadings[] = 'Year';
		$columnHeadings[] = 'Rider';
	} else if ($groupByAthlete) {
		$showSpecialty = true;
		$columnHeadings[] = 'Year';
		$columnHeadings[] = 'Championship';
		$columnHeadings[] = 'Category';
	}
	$newGroup = false;
	
	$isFirstGroup = true;
	foreach ($championsArray as &$currentChampion) {
		$columnData = Array();
		
		if (strcmp($currentChampion->specialty, $specialty) != 0) {
			continue;
		}
		if (strcmp($currentChampion->country, $locality) != 0 && !$groupByAthlete) {
			continue;
		}
		
		$cat = $categories[$currentChampion->category];
		if ($cat == null) {
			$cat = $currentChampion->category;
		}
		
		$currentRiderFullName = $currentChampion->firstname . ' ' . strtoupper($currentChampion->surname);
		if (NULL != $currentChampion->state && strcmp('', $currentChampion->state) != 0) {
			$state = ' (' . $currentChampion->state . ')';
		} else {
			$state = '';
		}
		
		if ($groupByYear && $lastGroupId != $currentChampion->year) {
			$newGroup = true;
			$lastGroupId = $currentChampion->year;
		} else if ($groupByCategory && strcmp($lastGroupId, $cat) != 0) {
			$newGroup = true;
			$lastGroupId = $cat;
		} else if ($groupByAthlete && strcmp($lastGroupId, $currentRiderFullName)) {
			$newGroup = true;
			$lastGroupId = $currentRiderFullName;
		}
		
		if ($newGroup) {
			if (!$isFirstGroup) {
				out ('</table>' . LF);
			}
			$isFirstGroup = false;
			out ('<a name="' . $lastGroupId . '" />' . LF);
			out ('<h2>' . $lastGroupId . '</h2>' . LF);
			out ('<table>' . LF);
			out ('<tr>' . LF);
			
			foreach($columnHeadings as $heading) {
				out (' <th>' . $heading . '</th>' . LF);
			}
			out ('</tr>' . LF);
		}
		
		if ($groupByYear) {
			if ($showSpecialty) {
				$columnData[] = $currentChampion->specialty;
			}
			$columnData[] = $cat;
			$columnData[] = $currentRiderFullName . $state;
		} else if ($groupByCategory) {
			if ($showSpecialty) {
				$columnData[] = $currentChampion->specialty;
			}
			$columnData[] = $currentChampion->year;
			$columnData[] = $currentRiderFullName . $state;
		} else if ($groupByAthlete) {
			$columnData[] = $currentChampion->year;
			$columnData[] = $localities[$currentChampion->country];
			$columnData[] = $cat;
		}
		
		out ('<tr>' . LF);

		foreach($columnData as $data) {
			out('  <td>' . $data . '</td>' . LF);
		}

		out ('</tr>' . LF);
		
		$newGroup = false;
	}
	
	out ('</table>' . LF);
?>